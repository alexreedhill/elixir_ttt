#Elixir Tic Tac Toe

Tic Tac Toe game built with the awesome new programming language, Elixir.

To play the game you must have Erlang and Elixir installed. This game is intended for use with Elixir 1.0.0. My preferred method is homebrew:

```
brew install elixir
```
To play the game, simply run the executable:

```
./elixir_ttt
```
To run the tests, use Elixir's build tool Mix:

```
mix test
```

You can also include the integration tests:

```
mix test --include external
```

If you'd like to make changes to the game, make sure you rebuild the executable:

```
mix escript.build
```

Have fun!
