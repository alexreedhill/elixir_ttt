defmodule ElixirTTT.AI do
  import Enum, only: [count: 1, reduce: 3, max: 1, min: 1, max_by: 2]
  import ElixirTTT.Util, only: [compact: 1, sample: 1]
  alias ElixirTTT.Board, as: Board
  alias ElixirTTT.Rules, as: Rules

  @min_rank -100
  @max_rank 100

  def next_move(board, ai_char, human_char) do
    if forceable_turn?(board) do
      force_first_move(board, ai_char)
    else
      board |> rank_nodes(ai_char, human_char) |> max_ranked_node
    end
  end

  defp forceable_turn?(board) do
    board |> compact |> count <= Board.height(board) - 2
  end

  defp force_first_move(board, ai_char) do
    middle_cell = Board.middle_cell(board)
    if Board.corner_taken?(board) && middle_cell do
      Board.fill(board, middle_cell, ai_char)
    else
      corner_cell = Board.empty_corner_cells(board) |> sample
      Board.fill(board, corner_cell, ai_char)
    end
  end

  defp rank_nodes(board, ai_char, human_char) do
    for cell <- Board.empty_cells(board) do
      node = Board.fill(board, cell, ai_char)
      rank = minimax(node, Board.height(board) + 1, ai_char, human_char, :min,
                     Rules.game_over?(node), @min_rank, @max_rank)
      { rank, node }
    end
  end

  defp minimax(node, _, ai_char, _, _, _, alpha, beta) when alpha >= beta, do: rank(node, ai_char)
  defp minimax(node, 0, ai_char, _, _, _, _, _), do: rank(node, ai_char)
  defp minimax(node, _, ai_char, _, _, true, _, _), do: rank(node, ai_char)
  defp minimax(node, depth, ai_char, human_char, :max, false, alpha, beta) do
    reduce Board.empty_cells(node), @min_rank, fn(cell, acc) ->
      if acc > alpha, do: alpha = acc
      child = Board.fill(node, cell, ai_char)
      rank = minimax(child, depth - 1, ai_char, human_char, :min,
                     Rules.game_over?(child), alpha, beta)
      max([rank, acc])
    end
  end
  defp minimax(node, depth, ai_char, human_char, :min, false, alpha, beta) do
    reduce Board.empty_cells(node), @max_rank, fn(cell, acc) ->
      if acc < beta, do: beta = acc
      child = Board.fill(node, cell, human_char)
      rank = minimax(child, depth - 1, ai_char, human_char, :max,
                     Rules.game_over?(child), alpha, beta)
      min([rank, acc])
    end
  end

  defp rank(board, ai_char) do
    board |> Rules.win? |> do_rank(ai_char)
  end

  defp do_rank({true, ai_char}, ai_char), do: 1
  defp do_rank({true, human_char}, ai_char), do: -1
  defp do_rank({false, _}, _), do: 0

  defp max_ranked_node(ranked_nodes) do
    max_by(ranked_nodes, fn(node) -> elem(node, 0) end) |> elem(1)
  end
end
