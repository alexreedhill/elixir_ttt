defmodule ElixirTTT.MockIO do
  import ElixirTTT.Util, only: [sample: 1]
  import String, only: [upcase: 1]
  import List, only: [wrap: 1]
  import ElixirTTT.Validations, only: [valid_ai_char?: 2]

  def greeting, do: ""

  def goodbye, do: "See you later!"

  def ready_prompt, do: sample(["y", "n"])

  def human_char_prompt, do: random_letter

  def ai_char_prompt(human_char) do
    ai_char = random_letter
    if valid_ai_char?(human_char, ai_char) do
      ai_char
    else
      ai_char_prompt(human_char)
    end
  end

  def first_player_prompt, do: sample([:human, :ai])

  def height_prompt, do: 3

  def thinking, do: ""

  def display_board(_), do: ""

  def next_move_prompt(_, nil), do: 0
  def next_move_prompt(_, previous_move), do: previous_move + 1

  def invalid_move, do: ""

  def loss_response, do: ""

  def tie_response, do: ""

  def play_again_prompt, do: sample(["n", "y"])

  def change_options_prompt, do: sample(["n", "y"])

  defp random_letter, do: 97..122 |> sample |> wrap |> to_string |> upcase


end
