defmodule ElixirTTT.Validations do
  import Enum, only: [map: 2, count: 1]
  import List, only: [wrap: 1]
  import String, only: [downcase: 1, to_integer: 1]
  alias ElixirTTT.Board, as: Board

  def valid_boolean?(boolean), do: boolean in ["y", "n"]

  def valid_human_char?(char) do
    letters = map(97..122, fn(n) -> wrap(n) end)
    (char |> downcase |> to_char_list) in letters
  end

  def valid_ai_char?(ai_char, human_char) do
    ai_char != human_char && valid_human_char?(ai_char)
  end

  def valid_first_player?(first_player), do: first_player in ["human", "ai"]

  def valid_height?(height), do: height in ["3", "4"]

  def valid_next_move?(board, next_move) do
    try do
      index = to_integer(next_move)
      index <= count(board) - 1 and Board.cell(board, index) |> Board.cell_empty?
    rescue
      ArgumentError -> false
    end
  end

end
