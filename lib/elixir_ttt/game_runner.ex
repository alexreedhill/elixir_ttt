defmodule ElixirTTT.GameRunner do
  alias ElixirTTT.GameIO, as: GameIO
  alias ElixirTTT.Board, as: Board
  alias ElixirTTT.AI, as: AI
  alias ElixirTTT.Rules, as: Rules

  def main(_), do: run(GameIO)

  def run(io) do
    if io.ready_prompt == "y", do: get_options("y", io, nil), else: io.goodbye
  end

  defp get_options("y", io, _) do
    human_char = io.human_char_prompt
    setup(%{human_char: human_char, ai_char: io.ai_char_prompt(human_char),
            first_player: io.first_player_prompt, height: io.height_prompt}, io)
  end
  defp get_options("n", io, options), do: setup(options, io)

  defp setup(options, io) do
    options[:height]
      |> Board.generate
      |> start(io, options[:first_player], options)
  end

  defp start(board, io, :ai, options), do: ai_turn(board, io, options)
  defp start(board, io, :human, options), do: human_turn(board, io, options)

  defp ai_turn(board, io, options) do
    io.thinking
    board = AI.next_move(board, options[:ai_char], options[:human_char])
    unless Rules.game_over?(board) do
      human_turn(board, io, options)
    else
      game_over(board, io, options)
    end
  end

  defp human_turn(board, io, options) do
    io.display_board(board)
    board = human_move(board, io, options[:human_char])
    unless Rules.game_over?(board) do
      ai_turn(board, io, options)
    else
      game_over(board, io, options)
    end
  end

  defp human_move(board, io, human_char, previous \\ nil) do
    Board.fill(board, io.next_move_prompt(board, previous), human_char)
  end

  defp game_over(board, io, options) do
    io.display_board(board)
    if Rules.win?(board) == {true, options[:ai_char]} do
      io.loss_response
    else
      io.tie_response
    end
    play_again(io, options)
  end

  defp play_again(io, options) do
    io.play_again_prompt |> new_game(io, options)
  end

  defp new_game("y", io, options) do
    io.change_options_prompt |> get_options(io, options)
  end
  defp new_game("n", io, _), do: io.goodbye
end
