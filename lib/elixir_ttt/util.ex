defmodule ElixirTTT.Util do
  import Enum, only: [filter: 2, to_list: 1]

  def compact(coll) do
    filter(coll, &(&1 != nil))
  end

  def sample(list) when is_list(list) do
    list
      |> length
      |> :random.uniform
      |> :lists.nth(list)
  end
  def sample(%Range{} = range), do: range |> to_list |> sample

end
