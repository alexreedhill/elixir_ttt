defmodule ElixirTTT.BoardFormatter do
  import Enum, only: [reduce: 3, with_index: 1]
  alias ElixirTTT.Board, as: Board

  def draw_board(board) do
    reduce with_index(board), "", fn(cell, acc) ->
      value = if elem(cell, 0) == nil, do: " ", else: elem(cell, 0)
      acc <> value <> (cell |> Board.cell_index |> divider(Board.height(board)))
    end
  end

  defp divider(index, height) when rem(index + 1, height) == 0, do: "\n"
  defp divider(_, _), do: "|"
end
