defmodule ElixirTTT.Board do
  import List, only: [duplicate: 2, replace_at: 3, flatten: 1]
  import Enum, only: [chunk: 2, with_index: 1, filter: 2, count: 1, map: 2, at: 2, reverse: 1]

  def generate(height), do: duplicate(nil, height * height)

  def rows(board), do: chunk(board, height(board))

  def columns(board) do
    board |> rows |> transpose
  end

  def diagonals(board) do
    [ diagonal(board), board |> reverse_rows |> diagonal ]
  end

  def cell(board, index) do
    board |> with_index |> at(index)
  end

  def fill(board, index, char) when is_integer(index) do
    replace_at(board, index, char)
  end
  def fill(board, cell, char) when is_tuple(cell) do
    replace_at(board, cell_index(cell), char)
  end

  def empty_cells(board) do
    board |> with_index |> filter fn(cell) -> cell_empty?(cell) end
  end

  def height(board) do
    board |> count |> :math.sqrt |> round
  end

  def cell_empty?(cell) do
    cell_value(cell) == nil
  end

  def corner_cells(board) do
    indexed_board = with_index(board)
    [at(indexed_board, 0),
     at(indexed_board, board |> height |> - 1),
     at(indexed_board, count(board) - height(board)),
     at(indexed_board, count(board) - 1)]
  end

  def corner_taken?(board) do
    board |> empty_corner_cells |> count < 4
  end

  def empty_corner_cells(board) do
    corner_cells(board) |> filter fn(cell) -> cell_empty?(cell) end
  end

  def middle_cell(board) do
    max_index = board |> count |> - 1
    if rem(max_index, 2) == 0, do: board |> with_index |> at(div(max_index, 2))
  end

  def cell_index(cell) do
    elem(cell, 1)
  end

  defp cell_value(cell) do
    elem(cell, 0)
  end

  defp transpose([[]|_]), do: []
  defp transpose(rows) do
    [ map(rows, &hd/1) | map(rows, &tl/1) |> transpose ]
  end

  defp diagonal(board) do
    map 0..((board |> height) - 1), fn(i) ->
      at((board |> rows), i) |> at(i)
    end
  end

  defp reverse_rows(board) do
    map(board |> rows, &reverse(&1)) |> flatten
  end
end
