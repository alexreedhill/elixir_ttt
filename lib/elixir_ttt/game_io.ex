defmodule ElixirTTT.GameIO do
  import IO, only: [puts: 1, gets: 1]
  import String, only: [upcase: 1, to_integer: 1, to_atom: 1, strip: 1, downcase: 1]
  import Enum, only: [reduce: 3, with_index: 1]
  import ElixirTTT.Validations
  alias ElixirTTT.Board, as: Board
  alias ElixirTTT.BoardFormatter, as: BoardFormatter

  def greeting, do: output "Welcome to ElixirTTT!"

  def goodbye, do: output "See you later!"

  def ready_prompt do
    ready = input "Are you ready to play? (y/n)"
    if valid_boolean?(ready), do: ready, else: (invalid_input; ready_prompt)
  end

  def invalid_input, do: output "That input was invalid. Please try again."

  def human_char_prompt do
    char = "Choose your character (A-Z):" |> input |> upcase
    if valid_human_char?(char), do: char, else: (invalid_input; human_char_prompt)
  end

  def ai_char_prompt(human_char) do
    char = "Choose computer character (A-Z excluding your character):" |> input |> upcase
    if valid_ai_char?(char, human_char) do
      char
    else
      invalid_input
      ai_char_prompt(human_char)
    end
  end

  def first_player_prompt do
    first_player = input "Choose who goes first (human or ai):"
    if valid_first_player?(first_player) do
      to_atom(first_player)
    else
      invalid_input
      first_player_prompt
    end
  end

  def height_prompt do
    height = input "Choose your board height (3 or 4):"
    if valid_height?(height) do
      to_integer(height)
    else
      invalid_input
      height_prompt
    end
  end

  def thinking, do: output "Thinking..."

  def display_board(board), do: board |> BoardFormatter.draw_board |> output

  def next_move_prompt(board, previous) do
    next_move = input "Enter an integer to make your next move (zero based):"
    if valid_next_move?(board, next_move) do
      to_integer(next_move)
    else
      invalid_input
      next_move_prompt(board, previous)
    end
  end

  def loss_response, do: output "Oh no, you lost!"

  def tie_response do
    output "The game ended in a tie! That's awesome, because honestly " <>
           "that's the best possible result for you"
  end

  def play_again_prompt, do: input "Would you like to play again?"

  def change_options_prompt, do: input "Would you like to change your game settings?"

  defp output(message), do: puts "\n\n\n#{message}\n\n"

  defp input(prompt), do: "#{prompt}\n" |> gets |> strip |> downcase

end
