defmodule ElixirTTT.Rules do
  import ElixirTTT.Board, only: [rows: 1, columns: 1, diagonals: 1]
  import ElixirTTT.Util, only: [compact: 1]
  import Enum, only: [reduce: 3, uniq: 1, at: 2, count: 1, reduce: 2, concat: 2]

  def win?(board) do
    reduce partitions(board), { false, nil }, fn(partition, acc) ->
      uniques = uniq(partition)
      if winning_partition?(uniques), do: { true, at(uniques, 0) }, else: acc
    end
  end

  def game_over?(board) do
    tie?(board) || board |> win? |> elem(0)
  end

  defp partitions(board) do
    reduce [rows(board), columns(board), diagonals(board)], fn(partition, acc) ->
      concat(partition, acc)
    end
  end

  defp winning_partition?(uniques) do
    count(uniques) == 1 and uniques |> compact |> count > 0
  end

  defp tie?(board) do
    board |> compact |> count == board |> count
  end

end
