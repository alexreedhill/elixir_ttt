defmodule ElixirTTT.ValidationsTest do
  use ExUnit.Case

  import ElixirTTT.Validations
  import ElixirTTT.TestHelper, only: [empty_board: 0]
  alias ElixirTTT.Board, as: Board

  test "validates y as boolean yes" do
    assert valid_boolean?("y")
  end

  test "validates n as boolean no" do
    assert valid_boolean?("n")
  end

  test "invalidates all other input boolean" do
    refute valid_boolean?("yes")
  end

  test "validates letter characters for human char" do
    assert valid_human_char?("A")
  end

  test "invalidates non-letter characters for human chars" do
    refute valid_human_char?(";")
  end

  test "validates letter characters for ai char" do
    assert valid_ai_char?("B", "A")
  end

  test "invalidates non-letter characters for ai char" do
    refute valid_ai_char?(":", "A")
  end

  test "invalidates letters that are used for ai char" do
    refute valid_ai_char?("A", "A")
  end

  test "validates human string for first player" do
    assert valid_first_player?("human")
  end

  test "validates ai atom for first player" do
    assert valid_first_player?("ai")
  end

  test "invalidates any other string for first player" do
    refute valid_first_player?("any_other")
  end

  test "validates height of 3" do
    assert valid_height?("3")
  end

  test "validates height of 4" do
    assert valid_height?("4")
  end

  test "invalidates all other input for height" do
    refute valid_height?("5")
  end

  test "validates integers that are within the range of the board size" do
    assert valid_next_move?(empty_board, "4")
  end

  test "invalidates integers that are outside the range of the board size" do
    refute valid_next_move?(empty_board, "9")
  end

  test "invalidates integers where that cell index is filled" do
    board = Board.fill(empty_board, 0, "X")
    refute valid_next_move?(board, "0")
  end

  test "invalidates input that cannot be converted to an integer" do
    refute valid_next_move?(empty_board, "foo")
  end

end
