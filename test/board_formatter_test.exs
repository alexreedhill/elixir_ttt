defmodule ElixirTTT.BoardFormatterTest do
  use ExUnit.Case

  import ElixirTTT.BoardFormatter
  import ElixirTTT.TestHelper
  alias ElixirTTT.Board, as: Board

  test "draws empty 3x3 board" do
    assert draw_board(empty_board) == " | | \n | | \n | | \n"
  end

  test "draws 3x3 board with cells filled" do
    board = empty_board |> Board.fill(0, "X") |> Board.fill(8, "O")
    assert draw_board(board) == "X| | \n | | \n | |O\n"
  end

  test "draws empty 4x4 board" do
    assert draw_board(empty_4x4_board) == " | | | \n | | | \n | | | \n | | | \n"
  end

  test "draws 4x4 board with cells filled" do
    board = empty_4x4_board |> Board.fill(0, "O") |> Board.fill(15, "X")
    assert draw_board(board) == "O| | | \n | | | \n | | | \n | | |X\n"
  end
end

