defmodule ElixirTTT.TestHelper do
  import ElixirTTT.Board, only: [generate: 1]

  def empty_board, do: generate(3)

  def empty_4x4_board, do: generate(4)

  def cats_game do
    ["X", "O", "X",
     "X", "O", "O",
     "O", "X", "X"]
  end
end

ExUnit.start()
ExUnit.configure exclude: [external: true]
