defmodule ElixirTTT.UtilTest do
  use ExUnit.Case

  import ElixirTTT.Util

  test "compact returns empty array if all values are nil" do
    assert compact([nil, nil, nil]) == []
  end

  test "compact returns array with non-nil values left" do
    assert compact([nil, "B", "C", nil]) == ["B", "C"]
  end

  test "sample only returns random element from list" do
    list = [1, 2, 3]
    other_list = [4, 5, 6]
    sample = sample(list)
    assert sample in list
    refute sample in other_list
  end

  test "sample returns random element from range" do
    range = 1..2
    other_range = 3..4
    sample = sample(range)
    assert sample in range
    refute sample in other_range
  end
end

