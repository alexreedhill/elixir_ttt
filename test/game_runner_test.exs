defmodule ElixirTTT.GameRunnerTest do
  use ExUnit.Case

  import ElixirTTT.GameRunner

  @tag external: true
  test "plays games" do
    for index <- 1..20 do
      IO.puts("\nPlaying game #{index}")
      assert run(ElixirTTT.MockIO) == "See you later!"
    end
  end
end


