defmodule ElixirTTT.MockIOTest do
  use ExUnit.Case

  import ElixirTTT.MockIO
  import ElixirTTT.TestHelper, only: [empty_board: 0]

  test "#greeting is empty stub" do
    assert greeting == ""
  end

  test "#goodbye returns goodbye message" do
    assert goodbye == "See you later!"
  end

  test "#ready_prompt returns y" do
    ready = ready_prompt
    assert ready == "y" or ready == "n"
  end

  test "#human_char_prompt returns a character" do
    assert is_binary human_char_prompt
  end

  test "#ai_char_prompt returns a character" do
    assert is_binary ai_char_prompt("X")
  end

  test "#first_player_prompt returns human" do
    first_player = first_player_prompt
    assert first_player == :human || first_player == :ai
  end

  test "#height_prompt returns human" do
    assert height_prompt == 3
  end

  test "#thinking is empty stub" do
    assert thinking == ""
  end

  test "#display_board is empty stub with one ignored argument" do
    assert display_board(nil) == ""
  end

  test "#next_move_prompt returns zero when there is no previous move" do
    assert next_move_prompt(empty_board, nil) == 0
  end

  test "#next_move_prompt returns one plus previous move" do
    assert next_move_prompt(empty_board, 0) == 1
  end

  test "#invalid_move is empty stub" do
    assert invalid_move == ""
  end

  test "#loss_response is empty stub" do
    assert loss_response == ""
  end

  test "#tie_response is empty stub" do
    assert loss_response == ""
  end

  test "#play_again_prompt returns n" do
    play_again = play_again_prompt
    assert play_again == "n" || play_again == "y"
  end

  test "#change_options_prompt returns n" do
    change_options = change_options_prompt
    assert change_options == "n" || change_options == "y"
  end

end
