defmodule ElixirTTT.RulesTest do
  use ExUnit.Case

  import ElixirTTT.Rules
  import ElixirTTT.TestHelper, only: [empty_board: 0, cats_game: 0]

  test "winning board in first row" do
    board = ["X", "X", "X",
             nil, nil, nil,
             nil, nil, nil]
    assert win?(board) == { true, "X" }
  end

  test "winning board in second row" do
    board = [nil, nil, nil,
             "X", "X", "X",
             nil, nil, nil]
    assert win?(board) == { true, "X" }
  end

  test "winning board with O in third row" do
    board = [nil, nil, nil,
             nil, nil, nil,
             "O", "O", "O"]
    assert win?(board) == { true, "O" }
  end

  test "winning board in column" do
    board = ["O", nil, nil,
             "O", nil, nil,
             "O", nil, nil]
    assert win?(board) == { true, "O" }
  end

  test "winning board in left diagonal" do
    board = ["X", nil, nil,
             nil, "X", nil,
             nil, nil, "X"]
    assert win?(board) == { true, "X" }
  end

  test "winning board in right diagonal" do
    board = [nil, nil, "X",
             nil, "X", nil,
             "X", nil, nil]
    assert win?(board) == { true, "X" }
  end

  test "incomplete board is not win" do
    assert win?(empty_board) == { false, nil }
  end

  test "cat's game is not win" do
    assert win?(cats_game) == { false, nil }
    assert win?(cats_game) == { false, nil }
  end

  test "game is over on cats game" do
    assert game_over?(cats_game)
  end

  test "game is over on win" do
    board = ["X", "X", "X",
             nil, nil, nil,
             nil, nil, nil]
    assert game_over?(board)
  end
end
