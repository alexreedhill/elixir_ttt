defmodule ElixirTTT.BoardTest do
  use ExUnit.Case

  import ElixirTTT.Board
  import ElixirTTT.TestHelper, only: [empty_board: 0, empty_4x4_board: 0]
  import Enum, only: [count: 1]

  test "generates empty 3x3 board" do
    assert generate(3) == empty_board
  end

  test "generates empty 4x4 board" do
    assert generate(4) |> count == 16
  end

  test "returns rows" do
    assert rows(empty_board) == [[nil, nil, nil],
                                 [nil, nil, nil],
                                 [nil, nil, nil]]
  end

  test "returns columns" do
    board = ["X", nil, nil,
             "X", nil, nil,
             "X", nil, nil]

    assert columns(board) == [["X", "X", "X"],
                              [nil, nil, nil],
                              [nil, nil, nil]]
  end

  test "returns diagonals" do
    board = ["X", nil, "O",
             nil, "X", nil,
             "O", nil, "X"]

   assert diagonals(board) == [["X", "X", "X"],
                               ["O", "X", "O"]]
  end

  test "fills cell" do
    assert fill(empty_board, 0, "X") == ["X", nil, nil,
                                         nil, nil, nil,
                                         nil, nil, nil]
  end

  test "returns empty cells with index" do
    assert empty_cells(empty_board) == [{nil, 0}, {nil, 1}, {nil, 2},
                                        {nil, 3}, {nil, 4}, {nil, 5},
                                        {nil, 6}, {nil, 7}, {nil, 8}]
  end

  test "empty cells doesn't return filled cells with index" do
    board = fill(empty_board, 4, "X")
    assert empty_cells(board) == [{nil, 0}, {nil, 1}, {nil, 2},
                                  {nil, 3},           {nil, 5},
                                  {nil, 6}, {nil, 7}, {nil, 8}]
  end

  test "height" do
    assert height(empty_board) === 3
  end

  test "returns cell with index" do
    assert cell(empty_board, 0) == {nil, 0}
  end

  test "cell is empty if it is nil" do
    assert empty_board |> cell(0) |> cell_empty?
  end

  test "cell is not empty if it is not nil" do
    board = fill(empty_board, 0, "X")
    refute board |> cell(0) |> cell_empty?
  end

  test "corner cells for 3x3 board" do
    board = ["A", nil, "B",
             nil, nil, nil,
             "C", nil, "D"]

    assert corner_cells(board) == [{"A", 0}, {"B", 2}, {"C", 6}, {"D", 8}]
  end

  test "corner cells for 4x4 board" do
    board = ["A", nil, nil, "B",
             nil, nil, nil, nil,
             nil, nil, nil, nil,
             "C", nil, nil, "D"]

    assert corner_cells(board) == [{"A", 0}, {"B", 3}, {"C", 12}, {"D", 15}]
  end

  test "corner is taken" do
    board = ["X", nil, nil,
             nil, nil, nil,
             nil, nil, nil]

    assert corner_taken?(board)
  end

  test "corner is not taken" do
    refute corner_taken?(empty_board)
  end

  test "middle cell for 3x3" do
    assert middle_cell(empty_board) == {nil, 4}
  end

  test "middle cell for 4x4 false" do
    assert middle_cell(empty_4x4_board) == nil
  end

end
