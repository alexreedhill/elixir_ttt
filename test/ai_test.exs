defmodule ElixirTTT.AITest do
  use ExUnit.Case

  import ElixirTTT.AI
  import ElixirTTT.TestHelper
  import Enum, only: [count: 1]
  alias ElixirTTT.Board, as: Board

  test "makes first move in random corner" do
    board = next_move(empty_board, "X", "O")
    assert Board.corner_taken?(board)
  end

  test "blocks to prevent loss next turn" do
    board = ["X", "O", "X",
             nil, "O", nil,
             nil, nil, nil]
    assert next_move(board, "X", "O") == Board.fill(board, 7, "X")
  end

  test "wins next turn" do
    board = ["X", "O", "X",
             "X", "O", "O",
             nil, nil, nil]
    assert next_move(board, "X", "O") == Board.fill(board, 6, "X")
  end

  test "blocks fork" do
    board = [nil, nil, "X",
             nil, "O", nil,
             "X", nil, nil]

    assert next_move(board, "O", "X") == Board.fill(board, 1, "O")
  end

  test "responds to corner move correctly when going first" do
    board = ["O", nil, nil,
             nil, nil, nil,
             nil, nil, "X"]

    assert next_move(board, "O", "X") == Board.fill(board, 2, "O")
  end

  test "responds to corner move in middle when going second" do
    board = ["X", nil, nil,
             nil, nil, nil,
             nil, nil, nil]

    assert next_move(board, "O", "X") == Board.fill(board, 4, "O")
  end

  test "forces first two moves in corner for 4x4 board" do
    board = next_move(empty_4x4_board, "O", "X") |> Board.fill(14, "X") |> next_move("O", "X")
    assert board |> Board.empty_corner_cells |> count == 2
  end

  test "ai does not overwrite cells that are already taken when forcing first moves" do
    board = [nil, nil, nil, "X",
             nil, nil, nil, nil,
             nil, nil, nil, nil,
             nil, nil, nil, nil]

    assert next_move(board, "O", "X") |> Enum.at(3) == "X"
  end

end
