defmodule ElixirTTT.GameIOTest do
  use ExUnit.Case

  import ExUnit.CaptureIO
  import ElixirTTT.GameIO

  test "outputs message with newline" do
    assert capture_io(fn ->
      goodbye
    end) == "\n\n\nSee you later!\n\n\n"
  end
end
